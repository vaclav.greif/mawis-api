# # PurchaseUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_buy_id** | **int** | ID uživatele |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
