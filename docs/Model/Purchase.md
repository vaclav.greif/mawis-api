# # Purchase

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**\MawisApiClient\Model\PurchaseUser**](PurchaseUser.md) |  | [optional]
**order** | [**\MawisApiClient\Model\PurchaseOrder**](PurchaseOrder.md) |  | [optional]
**products** | [**\MawisApiClient\Model\PurchaseProductsInner[]**](PurchaseProductsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
