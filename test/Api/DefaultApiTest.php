<?php
/**
 * DefaultApiTest
 * PHP version 7.4
 *
 * @category Class
 * @package  MawisApiClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Synchronizace údajů z Mawis.eu do endpointů (WebApi)
 *
 * **Popis api pro synchronizaci** dat z Wordpress (*Mawis.eu*) na jednotlivé endpointy (*UR/MWP*) na které se budou odesílat data o **registrovaných a upravených uživatelích a provedených nákupech**. Adresy endpointů se nastavují v konfiguraci WordPress, viz administrační příručka.
 *
 * The version of the OpenAPI document: 1.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.4.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace MawisApiClient\Test\Api;

use \MawisApiClient\Configuration;
use \MawisApiClient\ApiException;
use \MawisApiClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * DefaultApiTest Class Doc Comment
 *
 * @category Class
 * @package  MawisApiClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class DefaultApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for confirmPost
     *
     * Potvrzení registrace.
     *
     */
    public function testConfirmPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for purchasePost
     *
     * Odeslání objednávky.
     *
     */
    public function testPurchasePost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for userPost
     *
     * Registrace uživatele.
     *
     */
    public function testUserPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for usersIdPut
     *
     * Editace uživatelem/Editace účtu správa vstupní bod.
     *
     */
    public function testUsersIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
